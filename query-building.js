const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost:27017/example')
const Room = require('./models/Room')
const Buiding = require('./models/Building')

async function main () {
//   update
//   const room = await Room.findById('621a28d0e02f89130e377a09')
//   room.capacity = 20
//   room.save()
//   console.log(room)
  const room = await Room.findOne({ capacity: { $gte: 100 } })
  console.log(room)
  console.log('----------')
  const rooms = await Room.find({ capacity: { $gte: 100 } })
  console.log(rooms)
  const buildings = await Buiding.find({}).populate('rooms')
  console.log(JSON.stringify(buildings))
}

main().then(function () {
  console.log('Finish')
})
